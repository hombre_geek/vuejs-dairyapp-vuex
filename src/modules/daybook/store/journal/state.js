
export default () =>({
    isLoading: true,

    entries: [

        {
            id     : new Date().getTime(),
            date   : new Date().toDateString(),
            text   : 'Excepteur voluptate culpa non voluptate reprehenderit mollit magna anim ad eu ullamco fugiat in. Culpa duis ut pariatur commodo cupidatat qui laborum cillum enim mollit aute reprehenderit. Labore eiusmod sunt sunt non aliqua esse in officia. Lorem consequat et qui commodo ut ad esse Lorem. Proident culpa minim fugiat consequat do anim elit labore nostrud minim elit excepteur. Incididunt est aute magna ut eu magna do minim aliqua incididunt nisi.',
            picture: null
        },

        {
            id     : new Date().getTime() + 1000,
            date   : new Date().toDateString(),
            text   : 'Lorem ex in ipsum sint deserunt cillum aute consectetur. Ea do sunt consectetur tempor est exercitation esse minim irure ipsum excepteur. Culpa sunt esse laboris pariatur et nulla. Duis culpa pariatur elit occaecat id adipisicing occaecat mollit nulla pariatur ut consectetur dolore. Est tempor nostrud ex minim nostrud tempor exercitation aute aliqua esse occaecat do. Minim reprehenderit velit adipisicing incididunt qui consectetur non eu nulla. Labore irure laboris do Lorem cupidatat.',
            picture: null
        },

        {
            id     : new Date().getTime() + 2000,
            date   : new Date().toDateString(),
            text   : 'Laborum mollit elit nisi in irure duis voluptate esse ut exercitation magna dolore adipisicing consectetur. Cillum sint occaecat et irure aliquip in minim esse est laboris pariatur irure sunt. Exercitation qui do et esse enim duis pariatur officia ut.',
            picture: null
        },

    ]
})

