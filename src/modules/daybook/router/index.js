export default {
    name: 'daybook',
    component: () => import(/* webpackChunkName DayBookLayout */ '@/modules/daybook/layouts/DayBookLayout'),
    children: [
        {
            path: '',
            name: 'no-entry-selected',
            component: () => import(/* webpackChunkName DayBookNoEntry */ '@/modules/daybook/views/NoEntrySelected')
        },
        {
            path: ':id',
            name: 'entry',
            component: () => import(/* webpackChunkName DayBookEntryView */ '@/modules/daybook/views/EntryView')
        }
    ]
}